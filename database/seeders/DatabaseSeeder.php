<?php

namespace Database\Seeders;

use App\Models\Cars;
use App\Models\Customers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $customersModel = new Customers();
        $carsModel = new Cars();

        $costumersIDs = [];
        $costumersIDs[] = $customersModel->addCustomer(//1
            "Иванов Иван Иванович",
            true,
            "+79999999999",
            "Волгоград, ул Ивана Ленина, д 30"
        );
        $costumersIDs[] = $customersModel->addCustomer(//2
            "Петров Пётр Иванович",
            true,
            "+79999123123",
            "Волгоград, ул Ивана Ленина, д 30"
        );
        $costumersIDs[] = $customersModel->addCustomer(//3
            "Сидоров Сергей Семёнович",
            true,
            "+79999333444",
            "Волгоград, ул Ивана Ленина, д 30"
        );
        /*$costumersIDs[] = $customersModel->addCustomer(//4
            "Иванов Иван Иванович",
            true,
            "+79999123399",
            "Волгоград, ул Ивана Ленина, д 30"
        );
        $costumersIDs[] = $customersModel->addCustomer(//5
            "Иванов Иван Иванович",
            true,
            "+79992222991",
            "Волгоград, ул Ивана Ленина, д 30"
        );
        $costumersIDs[] = $customersModel->addCustomer(//6
            "Иванов Иван Иванович",
            true,
            "+79992112999",
            "Волгоград, ул Ивана Ленина, д 30"
        );*/

        //1
        $carsModel->addCar(
            $costumersIDs[0],
            "Ceed 3",
            "Kia",
            "желтый",
            "a123аб",
            true
        );

        $carsModel->addCar(
            $costumersIDs[0],
            "Ceed 2",
            "Kia",
            "зеленый",
            "a333аа",
            true
        );

        //2
        $carsModel->addCar(
            $costumersIDs[1],
            "Ceed 3",
            "Acura",
            "черный",
            "a444аа",
            true
        );

        $carsModel->addCar(
            $costumersIDs[1],
            "Ceed 2",
            "Audi",
            "синий",
            "б782аз",
            true
        );

        //3
        $carsModel->addCar(
            $costumersIDs[2],
            "Ceed 3",
            "Ford",
            "желтый",
            "a876ад",
            true
        );

        $carsModel->addCar(
            $costumersIDs[2],
            "Ceed 2",
            "BMW",
            "красный",
            "д748ап",
            true
        );

        // \App\Models\User::factory(10)->create();
    }
}
