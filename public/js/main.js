var customersList, carsList, carToggle, carState;

var removeList = [];

function removeCarById(id){
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/customers/remove/"+id, true);
    xhttp.setRequestHeader('X-CSRF-Token', document.querySelector("input[name=\"_token\"]").value);
    xhttp.send();
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState === 4) {
            var resp = JSON.parse(xhttp.responseText);
            if(resp.success=="true"){
                location.reload();
            }else{
                console.log(resp)
            }
        }
    }
}

function getCarInfo(id){
    var xhttp = new XMLHttpRequest();

    xhttp.open("GET", "/customers/"+customersList.value+"/cars/"+id, true);
    xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhttp.setRequestHeader('X-CSRF-Token', document.querySelector("input[name=\"_token\"]").value);
    xhttp.send();
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState === 4) {
            info = JSON.parse(xhttp.responseText);
            carState = document.getElementById("carState");
            carToggle = document.getElementById("carToggle");
            carState.innerText=(info.is_exist)?"Припаркована":"Не припаркована";
            carToggle.innerText=(info.is_exist)?"Убрать с парковки":"Припарковать";
        }
    }

}

function getCarsTitles(customerId){
    var xhttp = new XMLHttpRequest();

    xhttp.open("POST", "/customers/"+customerId+"/cars/titles/", true);
    xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhttp.setRequestHeader('X-CSRF-Token', document.querySelector("input[name=\"_token\"]").value);
    xhttp.send();
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState === 4) {
            carsList.innerHTML="";

            var resp = JSON.parse(xhttp.responseText);
            for(var i = 0;i<resp.length;i++){
                var newOption = document.createElement("option");
                newOption.setAttribute("value", resp[i].id);
                newOption.innerText=resp[i].brand+" "+resp[i].model+" "+resp[i].number;

                carsList.append(newOption)
            }
            getCarInfo(resp[0].id)
        }
    }
}

function setParking(id, stage){
    var xhttp = new XMLHttpRequest();

    xhttp.open("POST", "/customers/"+customersList.value+"/cars/"+id, true);
    xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhttp.setRequestHeader('X-CSRF-Token', document.querySelector("input[name=\"_token\"]").value);
    xhttp.send(JSON.stringify({
        stage: !stage,
    }));
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState === 4) {
            console.log(xhttp.responseText);
        }
    }
}

window.addEventListener("load",function (){
    customersList = document.getElementById("customersList")
    carsList = document.getElementById("carsList");
    carToggle = document.getElementById("carToggle");
    carState = document.getElementById("carState");
    if(customersList!=null){
        customersList.addEventListener("change", function (e){
            var customerID = customersList.value
            getCarsTitles(customerID);
        })
        carsList.addEventListener("change", function (e){
            var carID = carsList.value
            getCarInfo(carID);
        })
        carToggle.addEventListener("click", function(){
            var carID = carsList.value
            if(carToggle.innerText=="Припарковать"){
                carToggle.innerText="Убрать с парковки";
                carState.innerText="Припаркована";
                setParking(carID, false);
            }else{
                carToggle.innerText="Припарковать";
                carState.innerText="Не припаркована";
                setParking(carID, true);
            }

        });
    }
});
