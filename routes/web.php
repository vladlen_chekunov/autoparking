<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomersController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['XssSanitizer']], function () {

//parking list
    Route::get('/', [CustomersController::class, 'getParking']);

//Customers list
    Route::get('/customers', [CustomersController::class, 'getCustomers'])->name('customers_list');

//Customers add
    Route::get('/customers/add', [CustomersController::class, 'addCustomers']);
    Route::post('/customers/add', [CustomersController::class, 'addCustomersCheck']);

//Customer edit
    Route::get('/customers/edit/{id}', [CustomersController::class, 'editCustomers']);
    Route::post('/customers/edit/{id}', [CustomersController::class, 'editCustomersCheck']);

//Customer remove
    Route::post('/customers/remove/{id}', [CustomersController::class, 'removeCar']);

//Car title
    Route::post('/customers/{id}/cars/titles/', [CustomersController::class, 'getCarTitleByID']);

//Get/set car info
    Route::get('/customers/{costumer_id}/cars/{id}/', [CustomersController::class, 'getCarInfo']);
    Route::post('/customers/{costumer_id}/cars/{id}/', [CustomersController::class, 'setCarExistance']);
});
