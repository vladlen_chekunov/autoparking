require('./bootstrap');

//window.Vue = require("vue").default;
import Vue from 'vue';

import addCustomerForm from './components/customerForm.vue';

Vue.component('customer-form', require('./components/customerForm.vue').default);
Vue.component('customer-edit-form', require('./components/customerEditForm.vue').default)

const app = new Vue({
    el: '#app',
    /*components: {
        'addCustomerForm': addCustomerForm
    },
    template: "",*/
});
