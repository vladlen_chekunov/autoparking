@extends('layout')
@section('title', 'Редактирование клиента')
@section('content')
    <h1>Редактирование клиента</h1>

    <customer-edit-form
        full-name="{{$customer->full_name}}"
        sex="{{ ($customer->sex=="1")?"female":"male" }}"
        phone-number="{{$customer->phone}}"
        address="{{$customer->address}}"
        v-bind:cars="{{$cars}}"

        :id="{{$customer->id}}"
    ></customer-edit-form>
    @endsection
