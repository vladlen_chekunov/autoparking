@extends('layout')
@section('title', 'Автопарковка')
@section('content')
    @csrf
    <h1>Парковка</h1>
    <form action="/customers/changeState" method="post">
        <div class="form-group">
            <label for="customersList">Клиент:</label><br>
            <select name="" id="customersList">
                @foreach($customers_titles as $title)
                    <option value="{{$title->id}}">{{$title->full_name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="carsList">Машина:</label><br>
            <select name="" id="carsList">
                @foreach($cars_titles as $title)
                    <option value="{{$title->id}}">{{$title->brand}} {{$title->model}} {{$title->number}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="carsList">Состояние:</label><br>
            <span id="carState">
                {{($cars_titles->first()==true)?"Припаркована":"Не припаркована"/*->is_exist*/}}
            </span>
        </div>
        <div class="form-group">
            <a href="#" id="carToggle" class="btn btn-primary">{{($cars_titles->first()==true)?"Убрать с парковки":"Припарковать"/*->is_exist*/}}</a>
        </div>
    </form>
    <h1>На парковке</h1>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">ФИО</th>
            <th scope="col">Автомобиль</th>
            <th scope="col">Номер</th>
            <th scope="col">Изменить</th>
            <th scope="col">Удалить</th>
        </tr>
        </thead>
        <tbody>
        @foreach($customers as $customerCar)
            <tr>
                <td>{{$customerCar->full_name}}</td>
                <td>{{$customerCar->brand}} {{$customerCar->model}} {{$customerCar->color}}</td>
                <td>{{$customerCar->number}}</td>
                <td><a href="/customers/edit/{{$customerCar->id}}" class="btn btn-outline-primary">Редактировать</a></td>
                <td><a class="removeCar btn btn-outline-danger" href="#" data-id="{{$customerCar->car_id}}">Удалить</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            @for($i=0;$i<$pages_count;$i++)
                @if($i+1==$current_page)
                    <li class="page-item active"><a class="page-link">{{$i+1}}</a></li>
                @else
                    <li class="page-item"><a class="page-link" href="./?page={{$i+1}}">{{$i+1}}</a></li>
                @endif
            @endfor
        </ul>
    </nav>
@endsection
