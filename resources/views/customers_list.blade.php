@extends('layout')
@section('title')Список автомобилей@endsection
@section('content')
    @csrf
    <h1>Клиенты</h1>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">ФИО</th>
                <th scope="col">Автомобиль</th>
                <th scope="col">Номер</th>
                <th scope="col">Изменить</th>
                <th scope="col">Удалить</th>
            </tr>
        </thead>
        <tbody>
        @foreach($customers as $customerCar)
            <tr>
                <td>{{$customerCar->full_name}}</td>
                <td>{{$customerCar->brand}} {{$customerCar->model}} {{$customerCar->color}}</td>
                <td>{{$customerCar->number}}</td>
                <td><a href="/customers/edit/{{$customerCar->id}}" class="btn btn-outline-primary">Редактировать</a></td>
                <td><a class="removeCar btn btn-outline-danger" href="#" data-id="{{$customerCar->car_id}}" class="btn btn-outline-danger">Удалить</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            @for($i=0;$i<$pages_count;$i++)
                @if($i+1==$current_page)
            <li class="page-item active"><a class="page-link">{{$i+1}}</a></li>
                @else
            <li class="page-item"><a class="page-link" href="./?page={{$i+1}}">{{$i+1}}</a></li>
                @endif
            @endfor
        </ul>
    </nav>
@endsection
