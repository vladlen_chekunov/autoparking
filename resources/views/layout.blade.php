<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <script type="text/javascript" src="/js/main.js"></script>
</head>
<body>
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
    <a href="/" class="my-0 mr-md-auto font-weight-normal text-dark">Автопарковка</a>
    <nav class="mr-md-3">
        <a href="/" class="p-2 my-0 mr-md-auto font-weight-normal text-dark">Парковка</a>
        <a href="/customers/" class="p-2 my-0 mr-md-auto font-weight-normal text-dark">Клиенты</a>
    </nav>
    <a href="/customers/add" class="btn btn-primary">Добавить клиента</a>
</div>
<div id="app" class="container">
    @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif
    @yield('content')
</div>
<script type="text/javascript" src="/js/app.js"></script>
</body>
</html>
