<?php

namespace App\Http\Controllers;

use App\Models\Cars;
use App\Models\Customers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Validator;
use Session;
use Response;

/*
 *
    The Laravel query builder uses PDO parameter binding to protect your application against SQL injection attacks. There is no need to clean strings being passed as bindings.
 *
 * */

class CustomersController extends Controller
{
    public function getCarTitleByID($id){
        $carModel = new Cars();
        $cars_titles = $carModel->getTitlesList($id);

        return response()->json($cars_titles);
    }

    public function getParking(Request $req){
        $customerModel = new Customers();
        $carModel = new Cars();

        $page = $req->input('page')?:1;
        $per_page = 2;

        $customers = $customerModel->getActiveCustomersList($page, $per_page);
        $customers_count = $customerModel->getActiveCustomersCount($page);

        $customers_titles = $customerModel->getTitlesList();
        $cars_titles = $carModel->getTitlesList($customers_titles->first()->id);//

        return view(
            'parking',
            [
                'customers_titles'=>$customers_titles,
                'cars_titles'=>$cars_titles,
                'customers'=>$customers,
                'pages_count'=>$customers_count/$per_page,
                'current_page'=>$page,
            ],
        );
    }

    public function getCustomers(Request $req){
        $customerModel = new Customers();

        $page = $req->input('page')?:1;
        $per_page = 2;

        $customers = $customerModel->getCustomersList($page, $per_page);
        $customers_count = $customerModel->getCustomersCount($page);

        return view(
            'customers_list',
            [
                'customers'=>$customers,
                'pages_count'=>$customers_count/$per_page,
                'current_page'=>$page
            ],
        );
    }

    public function addCustomers(){
        return view('customers_add');
    }

    public function editCustomersCheck(Request $req, $id){
        $validator = Validator::make($req->all(), [
            'fullName'=>'required|min:3|max:256',
            'sex'=>'required',
            'phone'=>'required|min:6|max:15',
            'address'=>'required|min:1|max:512',

            'carsUpdates.*.model'=>'required|min:2|max:512',
            'carsUpdates.*.brand'=>'required|min:2|max:512',
            'carsUpdates.*.color'=>'required|min:2|max:512',
            'carsUpdates.*.number'=>'required|min:2|max:512',

            'carsNew.*.model'=>'required|min:2|max:512',
            'carsNew.*.brand'=>'required|min:2|max:512',
            'carsNew.*.color'=>'required|min:2|max:512',
            'carsNew.*.number'=>'required|min:2|max:512',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages());
        }

        $customerModel = new Customers();
        $carModel = new Cars();

        $carsRemove = $req->input("carsRemove");
        foreach($carsRemove as &$car_id){
            $carModel->removeCarById($car_id);
        }
        
        $carsNew = $req->input("carsNew");
        foreach($carsNew as &$car) {
            if (!$carModel->isNumberUnique($car["number"])) {
                return response()->json((object)[
                    'errors' => [
                        "Номер машины должен быть уникальным",
                    ]
                ]);
            }
        }
        $carsUpdates = $req->input("carsUpdates");
        foreach($carsUpdates as &$car) {
            if (!$carModel->isNumberUnique($car["number"])) {
                return response()->json((object)[
                    'errors' => [
                        "Номер машины должен быть уникальным",
                    ]
                ]);
            }
        }

        $customerModel->editCustomer(
            $id,
            $req->input('fullName'),
            ($req->input('sex')=='female'),
            $req->input('phone'),
            $req->input('address')
        );

        foreach($carsNew as &$car) {
            $carModel->addCar(
                $id,
                $car["model"],
                $car["brand"],
                $car["color"],
                $car["number"],
                false,
            );
        }

        foreach($carsUpdates as &$car){
            $carModel->editCar(
                $car["id"],
                $car["model"],
                $car["brand"],
                $car["color"],
                $car["number"],
                false,
            );
        }


        Session::flash('message', 'Клиент успешно обновлён');
        return response()->json([
            "success"=>"true",
        ]);
    }

    public function addCustomersCheck(Request $req){
        $validator = Validator::make($req->all(), [
            'fullName'=>'required|min:3|max:256',
            'sex'=>'required',
            'phone'=>'required|min:6|max:15',
            'address'=>'required|min:1|max:512',
            'cars.*.model'=>'required|min:2|max:512',
            'cars.*.brand'=>'required|min:2|max:512',
            'cars.*.colour'=>'required|min:2|max:512',
            'cars.*.number'=>'required|min:2|max:512',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages());
        }

        $customerModel = new Customers();
        $carModel = new Cars();


        if(!$customerModel->isPhoneUnique($req->input('phone'))){
            return response()->json((object) [
                'errors'=>[
                    "Номер телефона должен быть уникальным",
                ]
            ]);
        }
        $cars = $req->input("cars");
        foreach($cars as &$car) {
            if (!$carModel->isNumberUnique($car["number"])) {
                return response()->json((object)[
                    'errors' => [
                        "Машина с таким номером ".$car["number"]." уже существует",
                    ]
                ]);
            }
        }

        $owner_id = $customerModel->addCustomer(
            $req->input('fullName'),
            ($req->input('sex')=='female'),
            $req->input('phone'),
            $req->input('address')
        );

        foreach($cars as &$car) {
            $carModel->addCar(
                $owner_id,
                $car["model"],
                $car["brand"],
                $car["colour"],
                $car["number"],
                false,
            );
        }

        Session::flash('message', 'Клиент успешно добавлен');
        return response()->json([
            "success"=>"true",
        ]);
    }

    public function editCustomers($id){
        $customersModel = new Customers();

        $data = $customersModel->getCustomerById($id);

        return view(
            'customers_edit',
            [
                'customer'=>$data["customer"],
                'cars'=>$data["cars"],
            ]
        );
    }


    public function removeCar($id){
        $carsModel = new Cars();
        $carsModel->removeCarById($id);
        Session::flash('message', 'Машина успешно удалена');
        return response()->json([
            "success"=>"true",
        ]);
    }

    public function getCarInfo($customer_id, $id){
        $carModel = new Cars();
        $info = $carModel->getCarInfoById($id);
        return response()->json($info);
    }

    public function setCarExistance(Request $req, $customer_id, $id){
        $carModel = new Cars();
        $carModel->setCarExistance($id, $req->input('stage'));
        return response()->json([
            "success"=>"true",
        ]);

    }
}
