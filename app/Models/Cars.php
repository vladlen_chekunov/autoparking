<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Cars extends Model
{
    public $timestamps = false;
    use HasFactory;
    public function addCar($owner_id, $model, $brand, $color, $number, $is_exist=false){
        DB::table('cars')->insert([
            'owner_id'=>$owner_id,
            'model'=>$model,
            'brand'=>$brand,
            'color'=>$color,
            'number'=>$number,
            'is_exist'=>$is_exist,
        ]);
    }

    public function editCar($id, $model, $brand, $color, $number, $is_exist=false){
        DB::table('cars')
            ->where('id', $id)
            ->update([
                'model'=>$model,
                'brand'=>$brand,
                'color'=>$color,
                'number'=>$number,
                'is_exist'=>$is_exist,
            ]);

    }

    public function isNumberUnique($number){
        return (DB::table('cars')->where('number', '=',$number)->limit(1)->count()==0);
    }

    public function removeCarById($id){
        DB::table('cars')->delete($id);
    }

    public function setCarExistance($id, $stage){
        DB::table('cars')->where('id', '=',$id)->update([
            'is_exist'=>$stage,
        ]);
    }

    public function getTitlesList($owner_id){
        $cars_titles = DB::table('cars')->select("id", "brand", "model", "number", "is_exist")->where('owner_id','=', $owner_id)->get();
        return $cars_titles;
    }

    public function getCarInfoById($id){
        $info = DB::table('cars')->select("*")->where("id","=", $id)->limit(1)->first();
        return $info;
    }
}
