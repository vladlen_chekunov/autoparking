<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Customers extends Model
{
    public $timestamps = false;
    use HasFactory;

    public function editCustomer($id, $full_name, $sex, $phone, $address){
        DB::table('customers')
            ->where('id', $id)
            ->update([
            'full_name'=>$full_name,
            'sex'=>$sex,
            'phone'=>$phone,
            'address'=>$address,
        ]);
    }

    public function addCustomer($full_name, $sex, $phone, $address){
        return DB::table('customers')->insertGetId([
            'full_name'=>$full_name,
            'sex'=>$sex,
            'phone'=>$phone,
            'address'=>$address,
        ]);
    }

    public function getCustomerById($id){
        $customer = DB::table('customers')->select("*")->where("id","=", $id)->limit(1)->first();
        $cars = DB::table('cars')->select("*")->where("owner_id","=", $id)->get();
        return [
            "customer" => $customer,
            "cars" => $cars,
        ];
    }

    public function getCustomersList($page, $perpage){
        $customers = DB::table('cars')->leftJoin('customers', 'cars.owner_id', '=', 'customers.id')->select("*")->addSelect('cars.id as car_id')->offset($perpage*($page-1))->limit($perpage)->get();
        return $customers;
    }

    public function getTitlesList(){
        $customers_titles = DB::table('customers')->select("id", "full_name")->get();
        return $customers_titles;
    }

    public function getActiveCustomersList($page, $perpage){
        $customers = DB::table('cars')->leftJoin('customers', 'cars.owner_id', '=', 'customers.id')->select("*")->addSelect('cars.id as car_id')->where('is_exist', true)->offset($perpage*($page-1))->limit($perpage)->get();
        return $customers;
    }

    public function getCustomersCount(){
        return DB::table('cars')->count();
    }

    public function getActiveCustomersCount(){
        return DB::table('cars')->where('is_exist', true)->count();
    }

    public function isPhoneUnique($number){
        return (DB::table('customers')->where('phone', '=',$number)->limit(1)->count()==0);
    }
}
